﻿namespace IdentityService.Web.DtoModels
{
    public class JwtTokenDto
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Role { get; set; }        

        public string Email { get; set; }
    }
}
