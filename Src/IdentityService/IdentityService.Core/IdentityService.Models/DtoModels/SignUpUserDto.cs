﻿namespace IdentityService.Models.DtoModels
{
    public class SignUpUserDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
