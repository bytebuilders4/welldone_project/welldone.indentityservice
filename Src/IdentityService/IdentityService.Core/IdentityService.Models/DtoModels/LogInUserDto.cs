﻿namespace IdentityService.Web.DtoModels
{
    public class LogInUserDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
