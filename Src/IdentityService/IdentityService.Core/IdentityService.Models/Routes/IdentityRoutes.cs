﻿namespace IdentityService.Models.Routes
{
    public class IdentityRoutes
    {
        public const string GetListUsers = "user/ids";
        public const string GetUser = "user/{id}";
        public const string CreateUser = "user";
        public const string UpdateUser = "user/{id}";
        public const string DeleteUser = "user/{id}";
        public const string GetToken = "authorize";
    }
}
