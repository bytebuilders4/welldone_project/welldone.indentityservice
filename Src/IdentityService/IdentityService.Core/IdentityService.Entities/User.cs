﻿namespace IdentityService.Entities
{
    public class User : BaseEntity<Guid>
    {
        public required string UserName { get; set; }
        public required string PasswordHash { get; set; }
        public required string Salt { get; set; }
        public required string Email { get; set; }
        public required string Role { get; set; }
        public void ChangeStatus(EntityStatus status) => Status = status;
    }
}
