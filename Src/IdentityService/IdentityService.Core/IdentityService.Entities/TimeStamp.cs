﻿using IdentityService.Interfaces.DataAccessInterfaces;

namespace IdentityService.Entities
{
    public class TimeStamp
    {
        public required string Initiator { get; set; }
        public required DateTimeOffset ChangeTime { get; set; }
        public required ChangeStatus Change { get; set; }
    }
}
