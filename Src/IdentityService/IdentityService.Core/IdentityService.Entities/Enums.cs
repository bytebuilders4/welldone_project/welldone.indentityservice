﻿namespace IdentityService.Entities
{
    public enum ChangeStatus
    {
        Created,
        Updated,
        Deactivated,
        Reactivated
    }

    public enum EntityStatus
    {
        Active,
        NotActive
    }
}
