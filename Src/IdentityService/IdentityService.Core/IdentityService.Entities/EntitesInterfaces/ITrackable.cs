﻿using IdentityService.Entities;

namespace IdentityService.Interfaces.DataAccessInterfaces
{
    public interface ITrackable
    {
        public List<TimeStamp>? TimeStamp { get; }
        void AddTimeStamp(string initiator, ChangeStatus status);
    }
}
