﻿using IdentityService.Interfaces.DataAccessInterfaces;

namespace IdentityService.Entities
{
    public class BaseEntity<TId> : ITrackable
    {
        public TId Id { get; set; }
        public List<TimeStamp> TimeStamp { get; } = [];
        public EntityStatus Status { get; protected set; }
        public void AddTimeStamp(string initiator, ChangeStatus status)
        {
            var timeStamp = new TimeStamp()
            {
                Initiator = initiator,
                Change = status,
                ChangeTime = DateTime.UtcNow
            };
            TimeStamp.Add(timeStamp);
        }
    }
}
