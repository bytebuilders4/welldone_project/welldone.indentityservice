using IdentityService.DataAccess.Implemetation;
using IdentityService.DataAccess.Postgress.Context;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.HttpContext;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Models.DtoModels;
using IdentityService.UseCases;
using IdentityService.UseCases.Identity.Commands.Authorize;
using IdentityService.UseCases.Identity.Commands.GetIdentity;
using IdentityService.UseCases.Mediator;
using IdentityService.UseCases.Security.GenerateHash;
using IdentityService.UseCases.Security.GetHash;
using IdentityService.UseCases.Security.GetTheHash;
using IdentityService.UseCases.UserCommands.CreateUser;
using IdentityService.UseCases.UserCommands.DeleteUser;
using IdentityService.UseCases.UserQueries.GetAll;
using IdentityService.UseCases.UserQueries.GetById;
using IdentityService.UseCases.UserQueries.GetByUserName;
using IdentityService.Web.DtoModels;
using IdentityService.Web.Mapper;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Identity.Configuration.Extensions;
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

//builder.AddServiceDefaults();

// Add services to the container.

builder.Services.AddControllers();
var connectionString = builder.Configuration["ConnectionStrings:PostgresConnection"];
builder.Services.AddDbContext<DataContext>(option =>
    option.UseNpgsql(connectionString, optionBuilder =>
        optionBuilder.MigrationsAssembly("IdentityService.DataAccess.Postgress")));

builder.Services.AddMassTransit(config =>
{
    config.SetKebabCaseEndpointNameFormatter();
    //TODO: ������� � ����� �����
    //var RabbitConfig = builder.Configuration.GetSection("RabbitMq");

    config.UsingRabbitMq((context, config) =>
    {

        config.Host(new Uri(builder.Configuration["RabbitMq:Host"]!), config =>
        {
            config.Username(builder.Configuration["RabbitMq:Username"]!);
            config.Password(builder.Configuration["RabbitMq:Password"]!);
        });

        config.ConfigureEndpoints(context);
    });

});


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IRepositoryBase<User, Guid>, UserRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddTransient<HashProducer>();
builder.Services.AddTransient<IUserContextService, UserContextService>();
builder.Services.AddJWTAuthentification();
builder.Services.ConfigureMapper();
builder.Services.AddScoped<IMediator, Mediator>();
builder.Services.AddTransient<IRequestHandler<GetAllUsersQuery, ICollection<User>>, GetAllUserQueryHandler>();
builder.Services.AddTransient<IRequestHandler<GetUserQuery, User?>, GetUserQueryHandler>();
builder.Services.AddTransient<IRequestHandler<GetByUserNameUserQuery, User?>, GetByUserNameQueryHandler>();
builder.Services.AddTransient<IRequestHandler<GetHashAndSaltCommand, (string, string)>, GetHashCommandHandler>();
builder.Services.AddTransient<IRequestHandler<GenerateHashCommand, string>, GenerateHashCommandHandler>();
builder.Services.AddTransient<IRequestHandler<DeleteUserByIdCommand, bool>, DeleteUserByIdCommandHandler>();
builder.Services.AddTransient<IRequestHandler<GetIdentityCommand, ClaimsIdentity?>, GetIdentityCommandHandler>();
builder.Services.AddTransient<IRequestHandler<AuthorizeUserCommand, JwtTokenDto?>, AuthorizeUserCommandHandler>();
builder.Services.AddTransient<IRequestHandler<CreateUserCommand, UserDto?>, CreateUserCommandHandler>();
var app = builder.Build();

//app.MapDefaultEndpoints();

DbInitializer.Initialize(app.Services);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
