﻿using AutoMapper;
using IdentityService.Entities;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Models.DtoModels;
using IdentityService.UseCases.Identity.Commands.GetIdentity;
using IdentityService.UseCases.UserCommands.CreateUser;
using IdentityService.UseCases.UserCommands.DeleteUser;
using IdentityService.UseCases.UserQueries.GetAll;
using IdentityService.UseCases.UserQueries.GetById;
using IdentityService.Web.DtoModels;
using IdentityService.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Identity.Configuration.Options;

namespace IdentityService.Web.Controllers
{
    [ApiController]
    [Route("identity")]
    public class UserController(
        AuthOptions authOptions,
        IMapper mapper,
        IMediator mediator)
        : ControllerBase
    {
        [HttpGet("users")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ICollection<UserDto>>> GetAllUsers()
        {
            var users = await mediator.Send(new GetAllUsersQuery());
            var result = Ok(mapper.Map<ICollection<UserDto>>(users));
            return result;
        }

        [HttpGet("user/{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<UserDto>> GetUser([FromRoute] Guid id)
        {
            var user = await mediator.Send(new GetUserQuery(){UserId = id});
            if(user is null) 
                return StatusCode( 404,"The user not found.");
            return Ok(mapper.Map<UserDto>(user));
        }

        [HttpDelete("user/{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteUser([FromRoute] Guid id)
        {
            var user = await mediator.Send(new GetUserQuery(){UserId = id});
            if (user is null)
                return NotFound("The user not found.");
            if (user.Status == EntityStatus.NotActive)
                return NotFound("The user has already been deleted.");
            var result = await mediator.Send(new DeleteUserByIdCommand() { Id = id });

            if (result)
                return Ok($"The user {user.UserName} has been deleted successfully.");
            else
                return StatusCode(500, "An error occurred while deleting the user.");
        }

        [HttpPost("user")]
        [AllowAnonymous]
        public async Task<ActionResult<UserDto>> CreateUser([FromBody] SignUpUserDto user)
        {
            var result = await mediator.Send(new CreateUserCommand() { User = user });

            if (result == null!)
                return Conflict("The user with this name already exists.");

            return Ok(mapper.Map<UserDto>(result));
        }

        [HttpPost("authorize")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> GetToken([FromBody] LogInUserDto user)
        {
            var identity = await mediator.Send(new GetIdentityCommand() { User = user });
            if (identity == null!)
                return BadRequest(new { errorText = "Invalid username or password." });
            var encodedJwt = TokenProducer.GetJWTToken(identity.Claims, authOptions);
            var response = new
            {
                token = encodedJwt,
                username = identity.Name
            };
            return Ok(response);
        }
    }
}
