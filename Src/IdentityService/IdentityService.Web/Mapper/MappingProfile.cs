﻿using AutoMapper;
using IdentityService.Entities;
using IdentityService.Models.DtoModels;
using IdentityService.Web.DtoModels;

namespace IdentityService.Web.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SignUpUserDto, User>()
                .ForMember(x => x.PasswordHash, map => map.Ignore())
                .ForMember(x => x.Salt, map => map.Ignore());
            CreateMap<User, UserDto>();
        }
    }
}
