﻿using AutoMapper;

namespace IdentityService.Web.Mapper
{
    public static class AutoMapperConfiguration
    {
        public static IServiceCollection ConfigureMapper(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAutoMapper(typeof(MappingProfile));
            return serviceCollection;
        }
    }
}