﻿using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.UserCommands.DeleteUser;

/// <summary>
/// Delete user by his Id
/// </summary>
public class DeleteUserByIdCommand() : IRequest<bool>
{
    public required Guid Id { get; set; }
}