﻿using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.UseCases.UserQueries.GetById;

namespace IdentityService.UseCases.UserCommands.DeleteUser;

public class DeleteUserByIdCommandHandler(
    IMediator mediator,
    IUnitOfWork unitOfWork) : IRequestHandler<DeleteUserByIdCommand, bool>
{
    public async Task<bool> Handle(DeleteUserByIdCommand request)
    {
        var user = await mediator.Send(
            new GetUserQuery() { UserId = request.Id });

        if (user is null)
            return false;

        await unitOfWork.Users.Delete(user);
        await unitOfWork.CompleteAsync();

        return true;
    }
}