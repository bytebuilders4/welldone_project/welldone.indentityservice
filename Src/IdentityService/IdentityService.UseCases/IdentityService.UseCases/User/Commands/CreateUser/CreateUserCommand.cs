﻿using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Models.DtoModels;

namespace IdentityService.UseCases.UserCommands.CreateUser;

public class CreateUserCommand() : IRequest<UserDto?>
{
    public required SignUpUserDto User { get; set; }
}