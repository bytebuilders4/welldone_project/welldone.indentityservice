﻿using AutoMapper;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Models.DtoModels;
using IdentityService.UseCases.Security.GetTheHash;
using IdentityService.UseCases.UserQueries.GetByUserName;
using MassTransit;
using WellDode.MassTransitModels;

namespace IdentityService.UseCases.UserCommands.CreateUser;

public class CreateUserCommandHandler(
    IMediator mediator,
    IPublishEndpoint publishEndpoint,
    IMapper mapper,
    IUnitOfWork unitOfWork) : IRequestHandler<CreateUserCommand, UserDto?>
{
    public async Task<UserDto?> Handle(CreateUserCommand request)
    {
        var user = await mediator.Send(
            new GetByUserNameUserQuery()
            {
                UserName = request.User.UserName
            });
        if (user is not null)
            return null!;

        (string Salt, string Hash) password = await mediator.Send(
            new GetHashAndSaltCommand() { Password = request.User.Password });

        User newUser = new User()
        {
            UserName = request.User.UserName,
            Email = request.User.Email,
            Role = "User",
            PasswordHash = password.Hash,
            Salt = password.Salt
        };
        newUser.ChangeStatus(EntityStatus.Active);

        newUser.Id = await unitOfWork.Users.Create(newUser);
        await unitOfWork.CompleteAsync();

        if (newUser.Id == Guid.Empty)
            return null!;

        await publishEndpoint.Publish(new CreateUserMessage { Id = newUser.Id, Email = newUser.Email, Username = newUser.UserName });

        return mapper.Map<UserDto>(newUser);
    }
}