﻿using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Entities;
using IdentityService.DataAccess.Implemetation;

namespace IdentityService.UseCases.UserQueries.GetAll;

public class GetAllUserQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetAllUsersQuery, ICollection<User>>
{
    public async Task<ICollection<User>> Handle(GetAllUsersQuery request)
    {
        var result = await unitOfWork.Users.Get(
            new UserQuery() { Filter = _ => true, Limit = 1000 });
        return result;
    }
}