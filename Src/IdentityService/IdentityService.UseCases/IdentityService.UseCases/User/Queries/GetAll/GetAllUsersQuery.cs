﻿using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Entities;

namespace IdentityService.UseCases.UserQueries.GetAll;

/// <summary>
/// Get all users
/// </summary>
public class GetAllUsersQuery() : IRequest<ICollection<User>>
{
}