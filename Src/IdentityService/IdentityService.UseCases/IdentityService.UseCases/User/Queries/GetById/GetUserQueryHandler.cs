﻿using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.UserQueries.GetById;

public class GetUserQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetUserQuery, User?>
{
    public async Task<User?> Handle(GetUserQuery request)
    {
        return await unitOfWork.Users.GetById(request.UserId);
    }
}