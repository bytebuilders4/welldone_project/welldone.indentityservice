﻿using IdentityService.Entities;
using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.UserQueries.GetById;

/// <summary>
/// Get user by his Id
/// </summary>
public class GetUserQuery() : IRequest<User?>
{
    public required Guid UserId { get; set; }
}