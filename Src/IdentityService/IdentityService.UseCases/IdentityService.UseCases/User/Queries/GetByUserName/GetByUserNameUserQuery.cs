﻿using IdentityService.Entities;
using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.UserQueries.GetByUserName;


/// <summary>
/// Get user by his Name
/// </summary>
public class GetByUserNameUserQuery() : IRequest<User?>
{
    public required string UserName { get; set; }
}