﻿using IdentityService.DataAccess.Implemetation;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.UserQueries.GetByUserName;

public class GetByUserNameQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetByUserNameUserQuery, User?>
{
    public async Task<User?> Handle(GetByUserNameUserQuery request)
    {
        var result = await unitOfWork.Users.Get(new UserQuery() { Filter = u => u.UserName == request.UserName, Limit = 1000});
        return result.SingleOrDefault();
    }
}