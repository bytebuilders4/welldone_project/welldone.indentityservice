﻿using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.Security.GenerateHash;

public class GenerateHashCommandHandler(HashProducer hp) : IRequestHandler<GenerateHashCommand, string>
{
    public Task<string> Handle(GenerateHashCommand request)
    {
        if (string.IsNullOrEmpty(request.Password))
            throw new ArgumentException("Invalid password");

        string theHash = hp.GeneratePasswordHash(request.Password, request.Salt);
        return Task.FromResult(theHash);
    }
}