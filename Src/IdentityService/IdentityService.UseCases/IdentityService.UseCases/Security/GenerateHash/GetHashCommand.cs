﻿using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.Security.GenerateHash;

/// <summary>
/// Get a hash for a password and salt
/// </summary>
public class GenerateHashCommand() : IRequest<string>
{
    public required string Password { get; set; }
    public required string Salt { get; set; }
}

