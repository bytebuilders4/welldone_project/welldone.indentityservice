﻿using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.Security.GetTheHash;

/// <summary>
/// Get a hash and salt for a password
/// </summary>
public class GetHashAndSaltCommand() : IRequest<(string,string)>
{
    public required string Password { get; set; }
}

