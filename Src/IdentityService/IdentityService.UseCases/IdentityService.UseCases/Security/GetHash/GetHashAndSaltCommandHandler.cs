﻿using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.UseCases.Security.GetTheHash;

namespace IdentityService.UseCases.Security.GetHash;

public class GetHashCommandHandler(HashProducer hp) : IRequestHandler<GetHashAndSaltCommand, (string,string)>
{
    public Task<(string, string)> Handle(GetHashAndSaltCommand request)
    {
        if (string.IsNullOrEmpty(request.Password))
            throw new ArgumentException("Invalid password");

        (string Salt, string Hash) result = hp.GetPasswordHash(request.Password);
        return Task.FromResult((result.Salt, result.Hash));
    }
}