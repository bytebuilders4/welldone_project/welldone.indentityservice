﻿using IdentityService.Interfaces.MediatorRInteraces;

namespace IdentityService.UseCases.Mediator
{
    public class Mediator(IServiceProvider serviceProvider) : IMediator
    {
        public async Task<TResponse> Send<TResponse>(IRequest<TResponse> request)
        {
            var handlerType = typeof(IRequestHandler<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            var handler = serviceProvider.GetService(handlerType);
            if (handler == null)
            {
                throw new InvalidOperationException($"Handler for '{request.GetType().Name}' not found.");
            }
            var method = handler.GetType().GetMethod("Handle");
            if (method == null)
            {
                throw new InvalidOperationException($"Handle method not found for '{handlerType.Name}'.");
            }

            var task = (Task<TResponse>)method.Invoke(handler, new object[] { request })!;

            return await task;
        }
    }
}
