﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace IdentityService.UseCases
{
    public class HashProducer
    {
        private string GetSalt()
        {
            var salt = RandomNumberGenerator.GetBytes(128 / 8);
            return Convert.ToBase64String(salt);
        }
        
        /// <summary>
        /// Generate a salt and hash from password
        /// </summary>
        /// <param name="password"></param>
        /// <returns>(salt,hash)</returns>
        /// <exception cref="ArgumentException"></exception>
        public (string, string) GetPasswordHash(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password is not valid");
            var salt = GetSalt();
            var hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8
                ));
            return (salt,hash);
        }

        /// <summary>
        /// Get the hash from password and salt
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns>Password hash</returns>
        /// <exception cref="ArgumentException"></exception>
        public string GeneratePasswordHash(string password, string salt)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password is not valid");
            if (string.IsNullOrEmpty(salt))
                throw new ArgumentException("Salt is not valid");

            var hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8
            ));
            return hash;
        }
    }
}
