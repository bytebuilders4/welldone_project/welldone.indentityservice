﻿using AutoMapper;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.UseCases.Security.GenerateHash;
using IdentityService.UseCases.UserQueries.GetByUserName;
using IdentityService.Web.DtoModels;

namespace IdentityService.UseCases.Identity.Commands.Authorize;

public class AuthorizeUserCommandHandler(IMapper mapper, IMediator mediator) : IRequestHandler<AuthorizeUserCommand, JwtTokenDto?>
{
    public async Task<JwtTokenDto?> Handle(AuthorizeUserCommand request)
    {
        if (string.IsNullOrEmpty(request.User.UserName) || string.IsNullOrEmpty(request.User.Password))
            return null;
        var user = await mediator.Send(new GetByUserNameUserQuery() { UserName = request.User.UserName });
        if (user is null)
            return null;
        var theHash = await mediator.Send(new GenerateHashCommand() { Password = request.User.Password, Salt = user.Salt });
        if (theHash != user.PasswordHash)
            return null;
        return new JwtTokenDto
        {
            UserName = user.UserName,
            Email = user.Email,
            Id = user.Id,
            Role = user.Role,
        };
    }
}