﻿using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Web.DtoModels;

namespace IdentityService.UseCases.Identity.Commands.Authorize;

public class AuthorizeUserCommand() : IRequest<JwtTokenDto?>
{
    public required LogInUserDto User { get; set; }
}