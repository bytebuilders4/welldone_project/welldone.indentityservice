﻿using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.Web.DtoModels;
using System.Security.Claims;

namespace IdentityService.UseCases.Identity.Commands.GetIdentity;

public class GetIdentityCommand() : IRequest<ClaimsIdentity?>
{
    public required LogInUserDto User { get; set; }
}