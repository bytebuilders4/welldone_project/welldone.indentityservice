﻿using AutoMapper;
using IdentityService.Interfaces.MediatorRInteraces;
using IdentityService.UseCases.Identity.Commands.Authorize;
using System.Security.Claims;

namespace IdentityService.UseCases.Identity.Commands.GetIdentity;

public class GetIdentityCommandHandler(IMapper mapper, IMediator mediator) : IRequestHandler<GetIdentityCommand, ClaimsIdentity?>
{
    public async Task<ClaimsIdentity?> Handle(GetIdentityCommand request)
    {
        var person = await mediator.Send(
            new AuthorizeUserCommand() { User = request.User });

        if (person is null) return null!;

        var claims = new List<Claim>
        {
            new Claim(ClaimsIdentity.DefaultNameClaimType, person.UserName),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role),
            new Claim("ID", person.Id.ToString()),
            new Claim("EMail",person.Email)
        };
        ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
        return claimsIdentity;
    }
}