﻿using System.Linq.Expressions;

namespace IdentityService.Interfaces.DataAccessInterfaces
{
    public interface IQuery<T> where T : class
    {
        public Expression<Func<T, bool>> Filter { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }

}
