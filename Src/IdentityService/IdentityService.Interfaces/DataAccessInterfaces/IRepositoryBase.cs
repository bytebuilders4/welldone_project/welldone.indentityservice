﻿using IdentityService.Web.DtoModels;

namespace IdentityService.Interfaces.DataAccessInterfaces
{
    public interface IRepositoryBase<T, TId> where T : class
    {
        Task<ICollection<T>> Get(IQuery<T> query);
        Task<T> GetById(TId id);
        Task<TId> Create(T entity);
        Task<TId> Update(T entity);
        Task Delete(T entity);
    }
}
