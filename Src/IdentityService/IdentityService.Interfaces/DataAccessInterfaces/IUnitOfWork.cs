﻿using IdentityService.Entities;

namespace IdentityService.Interfaces.DataAccessInterfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepositoryBase<User,Guid> Users { get; }
        Task<int> CompleteAsync();
    }
}
