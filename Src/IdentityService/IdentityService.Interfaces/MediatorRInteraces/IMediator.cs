﻿namespace IdentityService.Interfaces.MediatorRInteraces
{
    public interface IMediator
    {
        Task<TResponse> Send<TResponse>(IRequest<TResponse> request);
    }
}
