﻿namespace IdentityService.Interfaces.HttpContext
{
    public interface IUserContextService
    {
        string? GetCurrentUserName();
    }
}
