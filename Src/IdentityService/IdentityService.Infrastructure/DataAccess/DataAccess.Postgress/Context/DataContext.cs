﻿using IdentityService.DataAccess.Postgress.EntityConfiguration;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.HttpContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IdentityService.DataAccess.Postgress.Context
{
    public class DataContext(DbContextOptions<DataContext> options, IUserContextService contextService)
        : DbContext(options)
    {
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            TrackEntities();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            TrackEntities();
            return base.SaveChanges();
        }

        private void TrackEntities()
        {
            var entities = this.ChangeTracker
                .Entries<ITrackable>()
                .Where(e =>
                    e.State == EntityState.Added ||
                    e.State == EntityState.Modified)
                .ToArray();

            foreach (var entity in entities)
            {
                if(entity?.Entity is not ITrackable trackable)
                    continue;

                var status = entity.State == EntityState.Added
                    ? ChangeStatus.Created
                    : SpecifyModification(entity);

                var initiator = contextService.GetCurrentUserName() ?? "Undefined";
                trackable.AddTimeStamp(initiator, status);
            }


            ChangeStatus SpecifyModification(EntityEntry entity)
            {
                var oldStatus = entity.OriginalValues.GetValue<EntityStatus>("Status");
                var newStatus = entity.CurrentValues.GetValue<EntityStatus>("Status");

                return entity.State switch
                {
                    EntityState.Modified when oldStatus == EntityStatus.NotActive && newStatus == EntityStatus.Active => ChangeStatus.Reactivated,
                    EntityState.Modified when oldStatus == EntityStatus.Active && newStatus == EntityStatus.NotActive => ChangeStatus.Deactivated,
                    _ => ChangeStatus.Updated
                };
            };
        }
    }
}
