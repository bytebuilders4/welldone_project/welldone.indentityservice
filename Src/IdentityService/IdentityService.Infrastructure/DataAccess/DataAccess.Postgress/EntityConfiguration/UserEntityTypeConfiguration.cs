﻿using IdentityService.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IdentityService.DataAccess.Postgress.EntityConfiguration
{
    internal class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> userConfiguration)
        {
            //userConfiguration.ToTable("Users");
            userConfiguration.HasKey(u => u.Id);

            userConfiguration.OwnsMany(user => user.TimeStamp, OwnedNavigationBuilder =>
            {
                OwnedNavigationBuilder.ToJson();
            });
        }
    }
}
