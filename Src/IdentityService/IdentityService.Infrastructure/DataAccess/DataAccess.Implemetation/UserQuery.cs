﻿using System.Linq.Expressions;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;

namespace IdentityService.DataAccess.Implemetation;

public class UserQuery : IQuery<User>
{
    public required Expression<Func<User, bool>> Filter { get; set; } 
    public int Offset { get; set; }
    public int Limit { get; set; }
}