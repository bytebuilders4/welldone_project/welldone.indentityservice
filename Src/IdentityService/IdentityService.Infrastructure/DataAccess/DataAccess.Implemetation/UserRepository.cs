﻿using IdentityService.DataAccess.Postgress.Context;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.HttpContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IdentityService.DataAccess.Implemetation
{
    public class UserRepository(DataContext db, IUserContextService userContextService) : IRepositoryBase<User, Guid>
    {
        public async Task<Guid> Create(User entity)
        {
            await db.Users.AddAsync(entity);
            return entity.Id;
        }

        //TODO rename to SoftDelete?

        public async Task Delete(User entity)
        {
            var user = await db.Users.FindAsync(entity);
            if (user is null) return;
            user.ChangeStatus(EntityStatus.NotActive);
        }

        public async Task<ICollection<User>> Get(IQuery<User> query)
        {
            var userQuery = db.Users.AsQueryable();

            if (query?.Filter != null)
            {
                userQuery = userQuery.Where(query.Filter);
            }

            if (query.Offset > 0)
            {
                userQuery = userQuery.Skip(query.Offset);
            }

            if (query.Limit > 0)
            {
                userQuery = userQuery.Take(query.Limit);
            }

            return await userQuery.ToListAsync();
        }

        public async Task<User> GetById(Guid id)
        {
            var entity = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            return entity;
        }

        public async Task<Guid> Update(User entity)
        {
            db.Users.Update(entity);
            return entity.Id;
        }
    }
}
