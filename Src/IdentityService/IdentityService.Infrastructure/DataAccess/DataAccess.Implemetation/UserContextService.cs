﻿using IdentityService.Interfaces.HttpContext;
using Microsoft.AspNetCore.Http;

namespace IdentityService.DataAccess.Implemetation
{
    public class UserContextService(IHttpContextAccessor httpContextAccessor) : IUserContextService
    {
        public string? GetCurrentUserName()
        {
            return httpContextAccessor?.HttpContext?.User?.Identity?.Name;
        }
    }
}
