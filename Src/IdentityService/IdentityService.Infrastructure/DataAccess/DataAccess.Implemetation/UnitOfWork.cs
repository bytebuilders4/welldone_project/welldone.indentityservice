﻿using IdentityService.DataAccess.Postgress.Context;
using IdentityService.Entities;
using IdentityService.Interfaces.DataAccessInterfaces;
using IdentityService.Interfaces.HttpContext;
using Microsoft.EntityFrameworkCore;

namespace IdentityService.DataAccess.Implemetation
{
    public class UnitOfWork(DataContext context, IUserContextService userContextService) : IUnitOfWork, IAsyncDisposable
    {
        private IRepositoryBase<User, Guid> _userRepository = null!;

        public IRepositoryBase<User, Guid> Users => 
            _userRepository ??= new UserRepository(context, userContextService);

        public async Task<int> CompleteAsync()
        {
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public async ValueTask DisposeAsync()
        {
            await context.DisposeAsync();
        }
    }
}
