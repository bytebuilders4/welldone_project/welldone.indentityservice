﻿using IdentityService.DataAccess.Postgress.Context;
using IdentityService.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace IdentityService.DataAccess.Implemetation
{
    public static class DbInitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            var services = scope.ServiceProvider;
            try
            {
                var context = services.GetRequiredService<DataContext>();
                context.Database.Migrate();
                SeedData(context);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("An error occurred while migrating or initializing the database.");
                throw;
            }
        }

        private static void SeedData(DataContext context)
        {
            if (!context.Users.Any())
            {
                context.Users.AddRange(
                    new User
                    {
                        Id = new Guid("a24ab827-10a5-47bf-86bd-39adeb89b6c1"),
                        UserName = "Admin",
                        Salt = "RlooVG8iQKP645SkrEWN6g==",
                        PasswordHash = "qKsHCVxmj9xw8NXxQTbM+7oIHgArmo+g7kq36V2XFb4=",
                        Role = "Administrator",
                        Email = "admin@gmail.com",
                    },
                    new User
                    {
                        Id = new Guid("a24ab827-10a5-47bf-86bd-39adeb89b6c2"),
                        UserName = "User",
                        PasswordHash = "aSAU+4Dqb/hh4QmFlfUxEYTxGPWivyVufvKZVJZpLlA=",
                        Salt = "Aj+9bk3PidngW6J5FCl5Gg==",
                        Role = "User",
                        Email = "user@gmail.com",
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
